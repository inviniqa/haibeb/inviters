serve-dev:
	python manage.py runserver --settings=server.settings.dev 8000

serve-admin-dev:
	python manage.py runserver --settings=server.settings.admin.dev 8001 

serve-api-dev:
	python manage.py runserver --settings=server.settings.api.dev 8002

serve-website-dev:
	python manage.py runserver --settings=server.settings.websites.dev 8003
