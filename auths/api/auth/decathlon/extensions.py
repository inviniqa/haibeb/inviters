from drf_spectacular.extensions import OpenApiAuthenticationExtension
from drf_spectacular.plumbing import build_bearer_security_scheme_object


class DecathlonAuthenticationExtension(OpenApiAuthenticationExtension):
    target_class = (
        "authentics.api.auth.decathlon.authentication.DecathlonAuthentication"
    )
    name = "DecaAuth"
    match_subclasses = True
    priority = -1

    def get_security_definition(self, auto_schema):
        return build_bearer_security_scheme_object(
            header_name="Authorization",
            token_prefix=self.target.keyword,
        )
