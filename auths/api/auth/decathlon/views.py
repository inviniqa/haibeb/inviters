from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from .client import client
from .serializers import (
    DecathlonLoginSerializer,
    DecathlonObtainTokenSerializer,
    DecathlonRefreshTokenSerializer,
    DecathlonTokenSerializer,
)

User = get_user_model()


class DecathlonLoginURL(APIView):
    authentication_classes = []
    permission_classes = []

    @extend_schema(
        operation_id="decathlon_login",
        request=None,
        responses=DecathlonLoginSerializer,
    )
    def get(self, *args, **kwargs):
        """return login url for decathlon"""
        next = self.request.GET.get("next", None)
        return Response({"url": client.get_login_url(next)})


class DecathlonTokenObtainView(APIView):
    authentication_classes = []
    permission_classes = []

    def get_serializer(self, **kwargs):
        serializer = DecathlonObtainTokenSerializer(**kwargs)
        return serializer

    def get_response_serializer(self, **kwargs):
        serializer = DecathlonTokenSerializer(**kwargs)
        return serializer

    def get_serializer_context(self):
        return {}

    @extend_schema(
        operation_id="decathlon_obtain_token",
        request=DecathlonObtainTokenSerializer,
        responses=DecathlonTokenSerializer,
    )
    def post(self, *args, **kwargs):
        """Obtain access token"""
        serializer = self.get_serializer(
            data=self.request.data,
            context={"context": self.get_serializer_context()},
        )
        serializer.is_valid(raise_exception=True)
        resp = client.get_token(
            client.GRANT_AUTHORIZATION_CODE,
            auth_code=serializer.data["auth_code"],
        )
        if resp.status_code not in [200, 201]:
            raise ValidationError(resp.json())

        # extract data from token
        resp_json = resp.json()
        resp_serializer = self.get_response_serializer(data=resp_json)
        resp_serializer.is_valid(raise_exception=True)
        return Response(resp_serializer.data, status=200)


class DecathlonTokenRefreshView(APIView):
    authentication_classes = []
    permission_classes = []

    def get_serializer(self, **kwargs):
        serializer = DecathlonRefreshTokenSerializer(**kwargs)
        return serializer

    def get_response_serializer(self, **kwargs):
        serializer = DecathlonTokenSerializer(**kwargs)
        return serializer

    def get_serializer_context(self):
        return {}

    @extend_schema(
        operation_id="decathlon_refresh_token",
        request=DecathlonRefreshTokenSerializer,
        responses=DecathlonTokenSerializer,
    )
    def post(self, request, *args, **kwargs):
        """Obtain access token"""
        serializer = self.get_serializer(
            data=self.request.data,
            context={"context": self.get_serializer_context()},
        )
        serializer.is_valid(raise_exception=True)
        resp = client.get_token(
            client.GRANT_REFRESH_TOKEN,
            refresh_token=serializer.data["refresh_token"],
        )
        resp_json = resp.json()

        # check if response success
        if resp.status_code not in [200, 201]:
            return Response(resp_json, status=resp.status_code)

        # extract data from token
        access_token = resp_json["access_token"]
        user_info = client.extract_access_token(access_token)
        user_info_sub = user_info.get("sub", None)
        user_info_person = user_info.get("personid", None)
        try:
            User.objects.get(uid=user_info_sub)
        except User.DoesNotExist:
            # create user and get user info
            # user_info = client.get_identity(access_token)
            # user_contacts = client.get_contacts(access_token)
            User.objects.create(
                uid=user_info_sub,
                username=user_info_person,
                # first_name=user_info["firstname"],
                # last_name=user_info["lastname"],
                # email=user_contacts["email"],
            )
        # save user token for future use
        resp_serializer = self.get_response_serializer(data=resp_json)
        resp_serializer.is_valid(raise_exception=True)
        return Response(resp_serializer.data, status=200)
