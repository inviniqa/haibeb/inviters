import logging
from urllib import parse

import jwt
import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class DecathlonClient:
    BASE_URL = settings.DECATHLON_AUTH_BASEURL
    CLIENT_ID = settings.DECATHLON_CLIENT_ID
    CLIENT_SECRET = settings.DECATHLON_CLIENT_SECRET
    CLIENT_API_KEY = settings.DECATHLON_API_KEY
    REDIRECT_URI = settings.DECATHLON_REDIRECT_URI
    GRANT_REFRESH_TOKEN = "refresh_token"
    GRANT_AUTHORIZATION_CODE = "authorization_code"

    def get_login_url(self, next=None):
        url = DecathlonClient.BASE_URL + "/connect/oauth/authorize/?"

        # add next to redirect url
        redirect_url = DecathlonClient.REDIRECT_URI
        params = {
            "response_type": "code",
            "client_id": DecathlonClient.CLIENT_ID,
            "redirect_uri": redirect_url,
        }
        if next is not None:
            params.update({"next": next})

        return url + parse.urlencode(params)

    def get_token(self, grant_type, auth_code=None, refresh_token=None):
        url = DecathlonClient.BASE_URL + "/connect/oauth/token"
        params = {
            "client_id": self.CLIENT_ID,
            "client_secret": self.CLIENT_SECRET,
            "grant_type": grant_type,
        }
        if grant_type == self.GRANT_REFRESH_TOKEN and refresh_token is not None:
            params.update({"refresh_token": refresh_token})
        if grant_type == self.GRANT_AUTHORIZATION_CODE and auth_code is not None:
            params.update({"code": auth_code, "redirect_uri": self.REDIRECT_URI})
        resp = requests.get(url, params=params)
        return resp

    def obtain_access_token(self, auth_code=None):
        return self.get_token(
            grant_type=self.GRANT_AUTHORIZATION_CODE,
            auth_code=auth_code,
            refresh_token=None,
        )

    def refresh_access_token(self, refresh_token=None):
        return self.get_token(
            grant_type=self.GRANT_REFRESH_TOKEN,
            auth_code=None,
            refresh_token=refresh_token,
        )

    def extract_access_token(self, token):
        data = jwt.decode(token, options={"verify_signature": False})
        return data

    def get_optins(self, token):
        url = DecathlonClient.BASE_URL + "/sports_user/optins"
        headers = {
            "Authorization": f"Bearer {token}",
            "x-api-key": self.CLIENT_API_KEY,
        }
        resp = requests.get(url, headers=headers)
        return resp

    def get_contacts(self, token):
        url = DecathlonClient.BASE_URL + "/sports_user/contacts"
        headers = {
            "Authorization": f"Bearer {token}",
            "x-api-key": self.CLIENT_API_KEY,
        }
        resp = requests.get(url, headers=headers)
        return resp

    def get_identity(self, token):
        url = DecathlonClient.BASE_URL + "/sports_user/identity"
        headers = {
            "Authorization": f"Bearer {token}",
            "x-api-key": self.CLIENT_API_KEY,
        }
        resp = requests.get(url, headers=headers)
        return resp


client = DecathlonClient()
