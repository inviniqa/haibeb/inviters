from rest_framework import serializers


class DecathlonLoginSerializer(serializers.Serializer):
    url = serializers.URLField()


class DecathlonTokenSerializer(serializers.Serializer):
    access_token = serializers.CharField(required=True)
    refresh_token = serializers.CharField(required=True)

    def validate(self, attrs):
        data = super().validate(attrs)
        return data


class DecathlonObtainTokenSerializer(serializers.Serializer):
    auth_code = serializers.CharField(required=True)


class DecathlonRefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField(required=True)
