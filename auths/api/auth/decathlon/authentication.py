from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import APIException, AuthenticationFailed

from .client import client


class InvalidAuthorizationCode(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Invalid authorization code"
    default_code = "invalid_grant"


class NoDecathlonToken(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "No authentication token provided"
    default_code = "no_auth_token"


class InvalidDecathlonToken(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Invalid authentication token provided"
    default_code = "invalid_token"


class DecathlonAuthentication(BaseAuthentication):
    keyword = "Bearer"

    def authenticate(self, request):
        # check authorization header and extract auth token
        auth_header = request.META.get("HTTP_AUTHORIZATION")

        if not auth_header:
            msg = _("No authorization header provided.")
            raise AuthenticationFailed(msg)

        access_token = auth_header.split(sep=" ").pop()
        if access_token is None:
            msg = _("Authorization token is not provided.")
            raise InvalidDecathlonToken(msg)

        # Try to get identity, and check if token is valid
        resp_identity = client.get_identity(access_token)
        # print(resp_identity.json())

        if resp_identity.status_code not in [201, 200]:
            msg = _("Failed to get identity, access token is invalid.")
            raise InvalidDecathlonToken(msg)

        try:
            data = client.extract_access_token(access_token)
        except Exception:
            raise InvalidDecathlonToken(_("Failed to extract identity from token!"))

        try:
            sub = data.get("sub")
            personid = data.get("personid")
        except Exception:
            msg = _("Failed to get Sub ID from token.")
            raise AuthenticationFailed(msg)

        # Prepare minimum user data
        user_identity = {
            "pid": int(personid),
            "username": personid,
        }

        user, created = get_user_model().objects.get_or_create(
            uid=sub, defaults=user_identity
        )

        # TODO get email and phone number
        # Try update contacts
        # resp_contacts = client.get_contacts(access_token)
        # contacts = resp_contacts.json()
        # print(contacts)
        #
        # if resp_contacts.status_code in [201, 200]:
        #     email = contacts.get("email", None)
        #     if email is not None:
        #         user.email = email.get("value", None)
        #     user.save()

        return (user, None)
