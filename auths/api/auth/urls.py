from django.urls import path

from .jwt.views import JWTTokenObtainPairView, JWTTokenRefreshView

# from .decathlon.views import (
#     DecathlonLoginURL,
#     DecathlonTokenObtainView,
#     DecathlonTokenRefreshView,
# )

urlpatterns = [
    path("jwt/", JWTTokenObtainPairView.as_view(), name="jwt_obtain"),
    path("jwt/refresh/", JWTTokenRefreshView.as_view(), name="jwt_refresh"),
    # path("decathlon/login/", DecathlonLoginURL.as_view(), name="decathlon_login"),
    # path(
    #     "decathlon/obtain/",
    #     DecathlonTokenObtainView.as_view(),
    #     name="decathlon_obtain_token",
    # ),
    # path(
    #     "decathlon/refresh/",
    #     DecathlonTokenRefreshView.as_view(),
    #     name="decathlon_refresh_token",
    # ),
]
