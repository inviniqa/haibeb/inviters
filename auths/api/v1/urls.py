from rest_framework.routers import DefaultRouter

from server.api.helpers import get_router

from .viewsets import GroupViewSet, MembershipViewSet, PermissionViewSet, UserViewSet

router = DefaultRouter()
router.register("user", UserViewSet, "user")
router.register("group", GroupViewSet, "group")
router.register("permission", PermissionViewSet, "permission")
router.register("membership", MembershipViewSet, "membership")

urlpatterns = []

urlpatterns += router.urls
