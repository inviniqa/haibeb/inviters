from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from rest_framework import serializers

from auths.models import Membership
from invites.models import Envelope

User = get_user_model()


class MembershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membership
        fields = "__all__"


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = "__all__"


class EnvelopeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Envelope
        fields = [
            "amount",
            "usage_counter",
        ]


class UserSerializer(serializers.ModelSerializer):
    envelope = EnvelopeSerializer(read_only=True)
    membership = MembershipSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            "id",
            "uid",
            "username",
            # "email",
            "first_name",
            "last_name",
            "envelope",
            "membership"
        ]


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
        ]
