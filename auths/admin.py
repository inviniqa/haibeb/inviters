from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Membership


class MembershipModelAdmin(ModelAdmin):
    model = Membership


modeladmin_register(MembershipModelAdmin)
