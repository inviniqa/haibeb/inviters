from django import forms
from django.utils.translation import gettext_lazy as _
from wagtail.users.forms import UserCreationForm, UserEditForm

from .models import Membership


class CustomUserEditForm(UserEditForm):
    membership = forms.ModelChoiceField(
        queryset=Membership.objects, required=False, label=_("Membership")
    )


class CustomUserCreationForm(UserCreationForm):
    membership = forms.ModelChoiceField(
        queryset=Membership.objects, required=False, label=_("Membership")
    )
