# Generated by Django 4.1.5 on 2023-01-10 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("auths", "0002_membership_user_membership"),
    ]

    operations = [
        migrations.AlterField(
            model_name="membership",
            name="description",
            field=models.TextField(
                blank=True, max_length=512, null=True, verbose_name="description"
            ),
        ),
    ]
