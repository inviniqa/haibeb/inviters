from django.apps import AppConfig
from django.db.models.signals import post_migrate


class InvitesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "invites"

    def ready(self):
        post_migrate.connect(init_app, sender=self)
        return super().ready()


def init_app(sender, **kwargs):
    """For initializations"""
    from wagtail.models import Page

    from invites.models import Catalog

    root_page = Page.objects.get(slug="root")
    try:
        catalog_page = Catalog.objects.get(slug="catalog")
    except Catalog.DoesNotExist:
        catalog_page = Catalog(title="Catalog", slug="catalog")
        root_page.add_child(instance=catalog_page)
        catalog_page.save()
