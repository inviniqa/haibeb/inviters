from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Envelope


class EnvelopeModelAdmin(ModelAdmin):
    model = Envelope


modeladmin_register(EnvelopeModelAdmin)
