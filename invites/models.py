from django.apps import apps as django_apps
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http.response import Http404
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from wagtail.admin.panels import InlinePanel, FieldPanel
from wagtail.models import Orderable, Page, ParentalKey

from server.utils.slugs import unique_slugify
from websites.models import WebsiteSetting
from django.shortcuts import render

INVITATION_DEFAULT = "invites.WeddingInvitation"
INVITATION_MAPS = {
    "WeddingInvitation": "invites.WeddingInvitation",
    "GatheringInvitation": "invites.GatheringInvitation",
}

User = get_user_model()


class Catalog(Page):
    class Meta:
        verbose_name = _("Catalog")
        verbose_name_plural = _("Catalogs")


class Invitation(Page):

    data = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("data"),
    )

    content_panels = Page.content_panels + [
        FieldPanel("data"),
        InlinePanel("recipients"),
    ]

    class Meta:
        verbose_name = _("Invitation")
        verbose_name_plural = _("Invitations")

    @property
    def opts(self):
        return self.specific._meta

    @property
    def page_type(self):
        return f"{self.opts.app_label}.{self.specific.__class__.__name__}"

    @classmethod
    def get_invitation_model(cls, invitation_key):
        model_name = INVITATION_MAPS.get(invitation_key, None)
        if model_name is None:
            raise ValueError(_(f"{model_name} is not invitation type!"))
        return django_apps.get_model(model_name, require_ready=False)

    def get_template(self, request, template_name=None, *args, **kwargs):
        if request.headers.get("x-requested-with") == "XMLHttpRequest":
            return self.ajax_template or self.template
        website_settings = WebsiteSetting.for_request(request)
        if website_settings.theme is None:
            return "themes/default/blank.html"
        else:
            return website_settings.theme.get_template(template_name)

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context.update({"recipient": self.recipient})
        return context

    def serve(self, request, *args, **kwargs):
        from websites.models import WebsiteSetting

        self.request = request

        # Check Is Preview
        preview = request.GET.get("preview", False)
        if preview:
            self.recipient = {
                "name": "Jhone Doe",
                "address": "Jl. Soekarno Hatta",
                "city": "Jakarta",
            }
        else:
            # Is recipient set?
            to_query = request.GET.get("to", None)
            if to_query is None:
                # raise Http404("Recipient not set!")
                return render(
                    request,
                    self.get_template(request, "welcome.html"),
                    context={"message": "Recipient not set!"},
                )

            # Is recipient exist?
            self.recipient = self.recipients.filter(slug=to_query).first()
            if self.recipient is None:
                raise Http404("Recipient not found!")

            if not self.recipient.is_confirmed:
                raise Http404("Recipient not confirmed!")

        return super().serve(request, *args, **kwargs)


class Envelope(models.Model):
    PACKING_PRICE = 1
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name=_("user"),
    )
    amount = models.PositiveBigIntegerField(
        default=0,
        verbose_name=_("Amount"),
        help_text=_("Total envelope owned by user"),
    )
    usage_counter = models.PositiveIntegerField(
        default=0,
        verbose_name=_("Usage Counter"),
    )

    class Meta:
        verbose_name = _("Envelope")
        verbose_name_plural = _("Envelopes")

    def __str__(self) -> str:
        return f"{self.user}'s envelope"

    @classmethod
    def for_user(cls, user):
        envelope, _ = cls.objects.get_or_create(user=user)
        return envelope

    def pack(self, amount):
        if self.amount < amount:
            raise ValidationError({"envelope": _("Not enough envelope amount!")})
        else:
            self.amount = self.amount - amount
            self.usage_counter = self.usage_counter + amount
        self.save()

    def unpack(self, amount):
        if self.usage_counter < amount:
            raise ValidationError({"envelope": _("There is no envelope used!")})
        else:
            self.amount = self.amount + amount
            self.usage_counter = self.usage_counter - amount
        self.save()


class Recipient(Orderable, models.Model):
    slug = models.SlugField(_("slug"), editable=False)
    invitation = ParentalKey(
        Invitation,
        on_delete=models.CASCADE,
        related_name="recipients",
    )
    name = models.CharField(_("name"), max_length=50)
    email = models.EmailField(_("email"), null=True, blank=True)
    address = models.CharField(_("address"), max_length=250, null=True, blank=True)
    city = models.CharField(_("city"), max_length=250, null=True, blank=True)

    is_confirmed = models.BooleanField(default=False)

    email_sent = models.BooleanField(default=False)
    email_sent_time = models.DateTimeField(
        null=True, blank=True, verbose_name=_("email sent time")
    )

    @property
    def url(self):
        site = self.invitation.sites_rooted_here.first()
        if site is not None:
            url = f"{site.root_url}/?to={self.slug}"
        else:
            url = f"/?to={self.slug}"
        return url

    class Meta:
        verbose_name = _("Recipient")
        verbose_name_plural = _("Recipients")
        unique_together = ("email", "invitation")

    def __str__(self) -> str:
        return self.name

    def confirm(self):
        self.is_confirmed = True
        self.save()

    def send_email(self, envelope):
        # TODO Send email to recipient
        return

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, self.name)
        return super().save(*args, **kwargs)


class WeddingInvitation(Invitation):
    content_panels = Invitation.content_panels + []


class GatheringInvitation(Invitation):
    content_panels = Invitation.content_panels + []


@receiver(post_save, sender=User)
def create_envelope_after_create_user(sender, instance, *args, **kwargs):
    Envelope.for_user(instance)
