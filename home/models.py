from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from wagtail.models import Page

User = get_user_model()


class HomePage(Page):
    pass
