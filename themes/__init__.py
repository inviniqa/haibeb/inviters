import os

APP_DIR = __path__[0]
THEME_PATH = "themes"
THEMES_DIR = os.path.join(APP_DIR, "templates", THEME_PATH)
