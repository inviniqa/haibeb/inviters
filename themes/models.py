import os

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.contrib.settings.models import BaseSiteSetting, register_setting

from themes import THEME_PATH, THEMES_DIR

from .helpers import get_manifest, get_template_files

User = get_user_model()

TEMPLATE_DEFAULT = "index.html"


class Author(models.Model):
    owner = models.OneToOneField(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    name = models.CharField(_("name"), max_length=50)

    class Meta:
        verbose_name = _("author")
        verbose_name_plural = _("authors")

    def __str__(self) -> str:
        return self.name


class Theme(models.Model):
    name = models.CharField(_("name"), max_length=50)
    slug = models.SlugField(_("slug"), max_length=50, unique=True)
    description = models.TextField(_("description"))
    author = models.ForeignKey(
        Author,
        related_name="authors",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    @property
    def theme_path(self):
        return self.get_theme_path()

    def __str__(self) -> str:
        return self.name

    def get_manifest_file(self):
        path = self.theme_path
        return path

    def get_theme_path(self):
        return os.path.join(THEME_PATH, str(self.slug))

    def get_template(self, template_name=None):
        if template_name is None:
            template_name = TEMPLATE_DEFAULT
        return os.path.join(self.theme_path, template_name)
