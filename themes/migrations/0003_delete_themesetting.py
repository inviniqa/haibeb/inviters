# Generated by Django 4.1.5 on 2023-01-20 17:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("themes", "0002_author_theme_author"),
    ]

    operations = [
        migrations.DeleteModel(
            name="ThemeSetting",
        ),
    ]
