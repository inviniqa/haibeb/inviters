from drf_spectacular.utils import extend_schema
from rest_framework.viewsets import ReadOnlyModelViewSet

from themes.models import Theme

from .serializers import ThemeCreateSerializer, ThemeSerializer


class ThemeViewSet(ReadOnlyModelViewSet):
    serializer_class = ThemeSerializer

    def get_queryset(self):
        return Theme.objects.all()
