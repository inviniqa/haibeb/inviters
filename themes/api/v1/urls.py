from rest_framework.routers import DefaultRouter

from .viewsets import ThemeViewSet

router = DefaultRouter()
router.register("", ThemeViewSet, "theme")

urlpatterns = []

urlpatterns += router.urls
