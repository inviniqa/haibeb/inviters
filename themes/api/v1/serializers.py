from rest_framework import serializers

from auths.api.v1.serializers import UserSerializer
from themes.models import Author, Theme


class AuthorSerializer(serializers.ModelSerializer):
    # owner = UserSerializer(read_only=True)

    class Meta:
        model = Author
        fields = "__all__"


class ThemeSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()

    class Meta:
        model = Theme
        fields = "__all__"


class ThemeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = "__all__"
