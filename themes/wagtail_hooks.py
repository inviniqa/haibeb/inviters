from wagtail import hooks


@hooks.register("API_V1_URL_PATTERNS")
def register_themes_urls():
    return "themes/", "themes.api.v1.urls"
