from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Author, Theme


class ThemeModelAdmin(ModelAdmin):
    model = Theme


class AuthorModelAdmin(ModelAdmin):
    model = Author


modeladmin_register(ThemeModelAdmin)
modeladmin_register(AuthorModelAdmin)
