import json
import os

from themes import THEMES_DIR


def get_manifest(theme_name):
    manifest = os.path.join(THEMES_DIR, theme_name, "manifest.json")
    try:
        manifest_file = open(manifest, "r")
        manifest_data = json.loads(manifest_file.read())
    except Exception:
        raise FileNotFoundError(f"There is no manifest file in '{theme_name}' theme! ")
    return manifest_data


def get_template_files(theme_name):
    files = []
    themes_dir = os.path.join(THEMES_DIR, theme_name)

    def get_files(content_path, dir=None):
        for item in os.listdir(content_path):
            item_path = os.path.join(content_path, item)
            if (
                os.path.isfile(item_path)
                and not item.startswith("_")
                and item.endswith(".html")
            ):
                item_name = item.split(".")[0]
                if dir is not None:
                    item = os.path.join(dir, item)
                files.append({item_name: item})
            elif os.path.isdir(item_path):
                get_files(item_path, dir=item)

    get_files(themes_dir)
    return files
