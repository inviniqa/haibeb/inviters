# Wagtail settings

WAGTAIL_SITE_NAME = "server"

# Search
# https://docs.wagtail.org/en/stable/topics/search/backends.html
WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.database",
    }
}

# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
WAGTAILADMIN_BASE_URL = "http://example.com"

WAGTAIL_USER_EDIT_FORM = 'auths.forms.CustomUserEditForm'
WAGTAIL_USER_CREATION_FORM = 'auths.forms.CustomUserCreationForm'
WAGTAIL_USER_CUSTOM_FIELDS = ['membership']
