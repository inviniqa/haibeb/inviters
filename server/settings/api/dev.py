from ..dev import *  # NOQA
from ..dev import SPECTACULAR_SETTINGS, REST_FRAMEWORK

ROOT_URLCONF = "server.settings.api.urls"

INSTALLED_APPS = [
    "home",
    "search",
    "websites",
    "themes",
    "invites",
    "auths",
    # WAGTAIL
    "wagtail.contrib.redirects",
    "wagtail.contrib.settings",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail",
    "modelcluster",
    "taggit",
    "django_filters",
    # REST
    "rest_framework",
    "rest_framework_api_key",
    "drf_spectacular_sidecar",
    "drf_spectacular",
    "corsheaders",
    # DJANGO
    # "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.staticfiles",
]

REST_FRAMEWORK["DEFAULT_PARSER_CLASSES"] = [
    "rest_framework.parsers.JSONParser",
]
REST_FRAMEWORK["DEFAULT_AUTHENTICATION_CLASSES"] = [
    "rest_framework_simplejwt.authentication.JWTAuthentication",
]
REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] = [
    "rest_framework.renderers.JSONRenderer",
]
REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = [
    "rest_framework.permissions.IsAuthenticated",
    "rest_framework_api_key.permissions.HasAPIKey",
]

SPECTACULAR_SETTINGS["SCHEMA_PATH_PREFIX"] = r"/v[0-9]"
