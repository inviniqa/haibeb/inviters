from .base import *  # NOQA
from .restapi import *  # NOQA

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-m2#u##r33k%t_mzt6isq$v)w$1vo_kzo-1c4a-kf*89nlsvpr_"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


try:
    from .local import *  # NOQA
except ImportError:
    pass
