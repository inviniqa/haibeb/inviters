from ..dev import *  # NOQA
from ..dev import REST_FRAMEWORK

ROOT_URLCONF = "server.settings.admin.urls"

REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = [
    "rest_framework.permissions.IsAuthenticated",
]
