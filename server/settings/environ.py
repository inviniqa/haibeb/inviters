##############################################################################
# ENVIRONMENT VARIABLES
# List all enviroment variables used in this project.
# This list should be maintained manually.
##############################################################################
import os

import environ

PROJECT_NAME = os.environ.setdefault("DJANGO_PROJECT_NAME", "Haibeb Invitation")
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.dirname(PROJECT_DIR)

environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

env = environ.Env(
    DEBUG=(bool, False),
    USE_TLS=(bool, False),
    SECRET_KEY=(str, "***"),
    SITE_ID=(int, 1),
    SENTRY_ENV=(str, "local"),
    SENTRY_DSN=(str, ""),
    SITE_NAME=(str, "example"),
    SITE_DOMAIN=(str, "127.0.0.1:8000"),
    BASE_URL=(str, "http://127.0.0.1:8000"),
    DEPLOYMENT_ENV=(str, "docker"),
    CSRF_TRUSTED_ORIGINS=(list, ["http://127.0.0.1:8000"]),
    CORS_ALLOW_ALL_ORIGINS=(bool, 0),
    CORS_ALLOW_CREDENTIALS=(bool, 0),
    CORS_ALLOWED_ORIGINS=(list, []),
    CORS_ALLOWED_ORIGIN_REGEXES=(list, []),
    ALLOW_WILD_CARD=(bool, True),
    STORAGE_TYPE=(str, "whitenoise"),
    POSTGRES_HOST=(str, "127.0.0.1"),
    POSTGRES_PORT=(int, 5432),
    POSTGRES_DB=(str, "postgres"),
    POSTGRES_USER=(str, "postgres"),
    POSTGRES_PASSWORD=(str, "postgres"),
    REDIS_PASSWORD=(str, ""),
    REDIS_URL=(str, "redis://127.0.0.1:6379/0"),
    EMAIL_SUBJECT_PREFIX=(str, ""),
    SMTP_SENDER=(str, ""),
    SMTP_PASSWORD=(str, ""),
    SMTP_USERNAME=(str, ""),
    SMTP_PORT=(str, ""),
    SMTP_HOST=(str, ""),
    EMAIL_USE_TLS=(bool, True),
    AWS_ACCESS_KEY_ID=(str, ""),
    AWS_SECRET_ACCESS_KEY=(str, ""),
    AWS_S3_ENDPOINT_URL=(str, ""),
    AWS_STORAGE_BUCKET_NAME=(str, ""),
    AWS_S3_URL_PROTOCOL=(str, "https:"),
    AWS_S3_CUSTOM_DOMAIN=(str, ""),
    SEARCH_ENGINE=(str, "whoosh"),
    SEARCH_INDEX_NAME=(str, "django_search"),
)

BASE_URL = env("BASE_URL")
