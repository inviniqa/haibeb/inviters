from ..dev import *  # NOQA
from ..dev import REST_FRAMEWORK  # NOQA

INSTALLED_APPS = [
    "home",
    "search",
    "websites",
    "themes",
    "invites",
    "auths",
    # WAGTAIL
    "wagtail.contrib.redirects",
    "wagtail.contrib.settings",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail",
    "modelcluster",
    "taggit",
    "rest_framework_api_key",
    "django_filters",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = [
    "rest_framework.permissions.IsAuthenticated"
]

ROOT_URLCONF = "server.settings.websites.urls"
