from django.conf import settings
from django.urls import include, path, re_path

from wagtail import views
from wagtail.coreutils import WAGTAIL_APPEND_SLASH
from wagtail.documents import urls as wagtaildocs_urls

if WAGTAIL_APPEND_SLASH:
    serve_pattern = r"^((?:[\w\-]+/)*)$"
else:
    serve_pattern = r"^([\w\-/]*)$"

WAGTAIL_FRONTEND_LOGIN_TEMPLATE = getattr(
    settings, "WAGTAIL_FRONTEND_LOGIN_TEMPLATE", "wagtailcore/login.html"
)

urlpatterns = [
    path("documents/", include(wagtaildocs_urls)),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    re_path(serve_pattern, views.serve, name="wagtail_serve"),
]
