from .base import *  # NOQA
from .restapi import *  # NOQA

DEBUG = False

try:
    from .local import *  # NOQA
except ImportError:
    pass
