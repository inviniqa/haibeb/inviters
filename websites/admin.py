from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import WebsiteMembership


class WebsiteMembershipModelAdmin(ModelAdmin):
    model = WebsiteMembership


modeladmin_register(WebsiteMembershipModelAdmin)
