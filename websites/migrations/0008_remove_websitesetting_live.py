# Generated by Django 4.1.5 on 2023-01-21 12:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("websites", "0007_websitesetting_theme"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="websitesetting",
            name="live",
        ),
    ]
