from wagtail import hooks

from .api.v1.viewsets import MeSiteViewSet


@hooks.register("API_V1_URL_PATTERNS")
def register_site_urls():
    return "sites/", "websites.api.v1.urls"


@hooks.register("API_V1_ME_VIEWSET")
def register_me_site_viewset():
    return {
        "prefix": "sites",
        "viewset": MeSiteViewSet,
        "basename": "sites",
    }
