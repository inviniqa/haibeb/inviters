from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.contrib.settings.models import BaseSiteSetting, register_setting

from auths.models import Membership
from themes.models import Theme

User = get_user_model()


@register_setting
class WebsiteSetting(BaseSiteSetting):
    owner = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("owner"),
    )
    theme = models.ForeignKey(
        Theme,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    @classmethod
    def owned_by_user(cls, user):
        return cls.objects.filter(owner=user)

    @classmethod
    def website_creation_allowed(cls, user):
        owned_websites = cls.owned_by_user(user).count()
        website_rules = WebsiteMembership.for_user(user)
        return website_rules.max_number > owned_websites

    def toggle_publish(self):
        """Set the root page to publish / unpublish"""
        page = self.site.root_page
        page.live = not page.live
        page.save()
        return page


class WebsiteMembership(models.Model):
    membership = models.OneToOneField(
        Membership,
        on_delete=models.CASCADE,
        related_name="website_rules",
        null=True,
        blank=True,
    )
    max_number = models.PositiveIntegerField(
        default=1,
        verbose_name=_("max number"),
    )

    class Meta:
        verbose_name = _("website membership")
        verbose_name_plural = _("website memberships")

    def __str__(self) -> str:
        return f"{self.membership} website rules"

    @classmethod
    def for_user(cls, user):
        membership = Membership.for_user(user)
        rules = cls.for_membership(membership)
        return rules

    @classmethod
    def for_membership(cls, membership):
        rules, created = cls.objects.get_or_create(membership=membership)
        return rules
