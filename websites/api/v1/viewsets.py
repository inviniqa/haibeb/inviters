from django.db import transaction
from drf_spectacular.utils import extend_schema
from rest_framework import response, status
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed, ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework_api_key.permissions import HasAPIKey
from wagtail.models import Page

from invites.models import Envelope, Invitation
from websites.models import WebsiteSetting

from .serializers import (
    Recipient,
    RecipientCreateSerializer,
    RecipientSerializer,
    Site,
    SiteCreateSerializer,
    SiteSerializer,
    WebsiteSettingSerializer,
    WebsiteSettingUpdateSerializer,
    create_hostname,
)


class SiteViewSet(ReadOnlyModelViewSet):
    serializer_class = SiteSerializer

    def get_queryset(self):
        return Site.objects.filter(is_default_site=False)


class MeSiteViewSet(ModelViewSet):
    serializer_class = SiteSerializer

    def get_queryset(self):
        return Site.objects.filter(websitesetting__owner=self.request.user)

    def get_serializer_class(self):
        if self.action == "website_settings":
            if self.request.method == "GET":
                return WebsiteSettingSerializer
            elif self.request.method == "PUT":
                return WebsiteSettingUpdateSerializer
        return super().get_serializer_class()

    def perform_create(self, serializer):
        obj = serializer.save()
        return obj

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(request=SiteCreateSerializer, responses=SiteSerializer)
    def create(self, request, *args, **kwargs):

        # Validate User Permission Here
        serializer = SiteCreateSerializer(
            data=request.data,
            context=self.get_serializer_context(),
        )
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        InvitationModel = Invitation.get_invitation_model(data.pop("type"))

        # Create Invitation Page
        catalog = Page.objects.get(slug="catalog").specific
        invitation = InvitationModel(title=data["title"])
        catalog.add_child(instance=invitation)
        invitation.save()

        try:
            site_data = {
                "hostname": create_hostname(data["domain"]),
                "site_name": data["domain"],
                "root_page": invitation.id,
            }
            serializer = self.get_serializer(data=site_data)
            serializer.is_valid(raise_exception=True)
            site = self.perform_create(serializer)

            # Create Site Settings
            website_setting = WebsiteSetting.for_site(site=site)
            website_setting.owner = self.request.user
            website_setting.theme_id = data["theme"]
            website_setting.save()

        except Exception:
            # TODO Need logger here!
            invitation.delete()
            raise ValidationError({"non_field_errors": "Failed to create site!"})

        headers = self.get_success_headers(serializer.data)
        return response.Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    @action(methods=["POST"], url_path="toggle-publish", detail=True)
    def toggle_publish(self, request, pk, **kwargs):
        site = self.get_object()
        website_setting = WebsiteSetting.for_site(site)
        website_setting.toggle_publish()
        website_setting.save()
        return response.Response(
            SiteSerializer(instance=site).data,
            status=status.HTTP_200_OK,
        )

    @extend_schema(responses=RecipientSerializer)
    @action(methods=["GET"], url_path="recipient", detail=True)
    def recipient(self, request, pk, **kwargs):
        site = self.get_object()
        invitation = site.root_page.specific
        queryset = invitation.recipients.all()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RecipientSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = RecipientSerializer(instance=queryset, many=True)
        return response.Response(serializer.data)

    @extend_schema(request=RecipientCreateSerializer, responses=RecipientSerializer)
    @action(methods=["POST"], url_path="recipient/add", detail=True)
    def add_recipient(self, request, pk, **kwargs):
        site = self.get_object()
        invitation = site.root_page.specific
        data = request.data
        # Needed for unique together
        data["invitation"] = invitation
        serializer = RecipientCreateSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        recipient = Recipient(invitation=invitation, **serializer.data)
        recipient.save()

        resp_serializer = RecipientSerializer(instance=recipient)
        return response.Response(resp_serializer.data)

    @extend_schema(request=None, responses=None)
    @action(
        methods=["POST"],
        url_path="recipient/(?P<recipient_id>[^/.]+)/confirm",
        detail=True,
    )
    def confirm_recipient(self, request, pk, recipient_id, **kwargs):
        invitation = self.get_object().root_page.specific
        envelope = Envelope.for_user(request.user)
        recipient = invitation.recipients.filter(pk=recipient_id).first()

        if recipient is None:
            return response.Response(status=status.HTTP_404_NOT_FOUND)

        data = {"message": "recipient already confirmed!"}

        if not recipient.is_confirmed:
            with transaction.atomic():
                envelope.pack(Envelope.PACKING_PRICE)
                recipient.confirm()
                data = {"message": "recipient confirmed successfully!"}
        return response.Response(data, status=status.HTTP_200_OK)

    @extend_schema(request=None, responses=None)
    @action(
        methods=["POST"],
        url_path="recipient/(?P<recipient_id>[^/.]+)/send-email",
        detail=True,
    )
    def sent_invitation_by_email(self, request, pk, recipient_id, **kwargs):
        invitation = self.get_object().root_page.specific
        envelope = Envelope.for_user(request.user)
        recipient = invitation.recipients.filter(pk=recipient_id).first()

        if recipient is None:
            return response.Response(status=status.HTTP_404_NOT_FOUND)
        if not recipient.is_confirmed:
            raise ValidationError({"is_confirmed": "Please confirm your recipient!"})
        if recipient.email is None:
            raise ValidationError({"email": "Please input valid email address!"})

        recipient.send_email(envelope)

        return response.Response(
            {"message": f"Your invitation sent to {recipient.email}!"},
            status=status.HTTP_200_OK,
        )

    @extend_schema(methods=["GET"], responses=WebsiteSettingSerializer)
    @extend_schema(
        methods=["PUT"],
        request=WebsiteSettingUpdateSerializer,
        responses=WebsiteSettingSerializer,
    )
    @action(methods=["GET", "PUT"], url_path="settings", detail=True)
    def website_settings(self, request, *args, **kwargs):
        site = self.get_object()
        website_setting = WebsiteSetting.for_site(site=site)
        if request.method == "GET":
            return self._website_settings_retrieve(website_setting)
        elif request.method == "PUT":
            return self._website_settings_update(request, website_setting)
        else:
            raise MethodNotAllowed(method=request.method)

    def _website_settings_retrieve(self, website_setting):
        serializer = self.get_serializer(instance=website_setting, partial=True)
        return response.Response(serializer.data)

    def _website_settings_update(self, request, website_setting):
        serializer = self.get_serializer(
            instance=website_setting, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(serializer.data)
