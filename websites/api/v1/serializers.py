from collections import namedtuple

from rest_framework import serializers
from rest_framework.validators import ValidationError
from wagtail.api.v2.serializers import get_serializer_class as get_page_serializer_class
from wagtail.models.sites import Site

from auths.api.v1.serializers import UserSerializer
from invites.models import INVITATION_MAPS, Recipient
from themes.api.v1.serializers import ThemeSerializer
from themes.models import Theme
from websites.models import WebsiteSetting


def create_hostname(domain):
    return f"{domain}.invites.localhost"


class WebsiteSettingSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    theme = ThemeSerializer()

    class Meta:
        model = WebsiteSetting
        fields = "__all__"


class WebsiteSettingUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = WebsiteSetting
        fields = (
            "owner",
            "theme",
        )


class SiteCreateSerializer(serializers.Serializer):
    user = UserSerializer(read_only=True)
    domain = serializers.CharField()
    theme = serializers.IntegerField()
    title = serializers.CharField()
    type = serializers.ChoiceField(choices=list(INVITATION_MAPS.items()), required=True)

    def validate(self, attrs):
        errors = {}

        # validate website rules, raise errors as soon as possible
        request = self.context.get("request", None)
        allowed = WebsiteSetting.website_creation_allowed(request.user)
        if not allowed:
            errors.update(
                {"domain": "You have reach website limit, please upgrade membership!"}
            )
            raise ValidationError(errors)

        # validate domain
        domain = attrs["domain"]
        hostname = create_hostname(domain)
        site = Site.objects.filter(hostname=hostname).first()
        if site is not None:
            domain_error = errors.get("domain", [])
            domain_error.append("Domain name already taken!")
            errors["domain"] = domain_error

        # validate theme
        theme_id = attrs["theme"]
        theme = Theme.objects.filter(pk=theme_id).first()
        if theme is None:
            errors.update({"theme": "Theme doesn't exists!"})

        if bool(errors):
            raise ValidationError(errors)

        attrs["theme"] = theme.id
        return attrs


class SiteSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True, source="websitesetting.owner")
    theme = ThemeSerializer(read_only=True, source="websitesetting.theme")
    root_page_type = serializers.SerializerMethodField()

    class Meta:
        model = Site
        fields = (
            "id",
            "hostname",
            "port",
            "root_page",
            "root_page_type",
            "owner",
            "theme",
            "is_default_site",
        )

    def get_root_page_type(self, obj):
        return obj.root_page.specific.page_type


class RecipientSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Recipient
        fields = ("id", "invitation", "name", "email", "address", "city", "url")

    def get_url(self, obj):
        return obj.url


class RecipientCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipient
        fields = ("invitation", "name", "email", "address", "city")
