from rest_framework.routers import DefaultRouter

from .viewsets import SiteViewSet

router = DefaultRouter()
router.register("", SiteViewSet, "site")

urlpatterns = []

urlpatterns += router.urls
