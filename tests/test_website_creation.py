from pprint import pprint

from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from themes.models import Author, Theme
from .client import Client

User = get_user_model()


class TestWebsiteAPI(APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test-api-key")
        self.api = Client(client=self.client, api_key=key, base_url="/api/v1")
        self.theme_author = Author.objects.create(name="Test Author")
        self.theme = Theme.objects.create(
            name="Default", slug="default", author=self.theme_author
        )
        self.customer_user = User.objects.create_user(
            username="customer_user",
            password="customer_user",
        )
        return super().setUp()

    def test_create_website_with_valid_data(self):
        resp = self.api.obtain_token(username="customer_user", password="customer_user")
        access_token = resp.json().get("access_token")
        data = {
            "domain": "newdomain",
            "theme": 1,
            "title": "New Website",
            "type": "WeddingInvitation",
        }
        resp = self.api.create_website(access_token=access_token, **data)
        self.assertEqual(resp.status_code, 201)
