class Client:
    def __init__(self, client, api_key, base_url):
        self.api_key = api_key
        self.base_url = base_url
        self.client = client

    def obtain_token(self, username, password):
        data = {"username": username, "password": password}
        self.client.credentials(HTTP_X_API_KEY=self.api_key)
        resp = self.client.post(f"{self.base_url}/auth/jwt/", data=data)
        return resp

    def refresh_token(self, refresh_token):
        data = {"refresh_token": refresh_token}
        self.client.credentials(
            HTTP_X_API_KEY=self.api_key,
        )
        resp = self.client.post(f"{self.base_url}/auth/jwt/refresh/", data=data)
        return resp

    def create_website(self, access_token, **data):
        self.client.credentials(
            HTTP_AUTHORIZATION=f"JWT {access_token}",
            HTTP_X_API_KEY=self.api_key,
        )
        resp = self.client.post(f"{self.base_url}/me/sites/", data=data)
        return resp
